

$(document).ready(function(){

  //Mobile navigation
  $('#navOpen').click(function() {
    if ( $(this).hasClass('nav-open--active') ) {
      $(this).removeClass('nav-open--active');
      $('.nav_mobile').hide();
      // $('#header').animate({height: '86px'}, 1, function(){
      //   $('.nav_mobile').hide();
      // });
    } else {
      $(this).addClass('nav-open--active');
      $('.nav_mobile').show();
      // $('#header').animate({height: '215px'}, 1, function(){
      //   $('.nav_mobile').show();
      // });
    }
  });

  $('.nav__item_mobile > a').click(function(){
    $('#navOpen').removeClass('nav-open--active');
    $('#header').animate({height: '86px'}, 1, function(){
      $('.nav_mobile').hide();
    });
  });

  //Hide and show navigation
  $(window).scroll(function(){
    if ($(this).scrollTop() > 600) {
      $('#header').fadeIn(500);
    } else {
      $('#header').fadeOut(500);
    }
  });

  //Counter Up
  $('.counter').counterUp({
    delay: 10,
    time: 4000
  });

  //Toggle login popup
  $('.login-btn_desktop').on('click', function(){
    $('.login-window_desktop').slideToggle();
  });

  //Smooth scroll after click on menu item
  $('a.btn').click(function(event){
    if ( $(this).parent().hasClass('nav__item--career') ) {
      return 0;
    } else {
      event.preventDefault();
      $('html, body').animate({
          scrollTop: $( $.attr(this, 'href') ).offset().top
      }, 500);
    }
  });


});

function formValidate(){

  var name = $('.signup-form__name').val();
  var email = $('.signup-form__email').val();
  var lastname = $('.signup-form__lastname').val();
  var pass1 = $('#pass');
  var pass2 = $('#passRepeat');
  var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if ( name.length <= 0 || name === null || /^\s+$/.test(name) ) {
    $('.signup-form__name').addClass('signup-form_error');
    return false;
  } else if ( lastname.length <= 0 || lastname === null || /^\s+$/.test(lastname) ) {
    $('.signup-form__lastname').addClass('signup-form_error');
    return false;
  } else if ( email.length <=0 || email === null || !emailRegex.test(email) ) {
    $('.signup-form__email').addClass('signup-form_error');
    return false;
  } else if ( pass1.val() !== pass2.val() ) {
    pass2.addClass('signup-form_error');
    return false;
  } else {
    return true;
  }

}

//Remove form--error class on focus
$('.signup-form input').focus(function(){
  $(this).removeClass('signup-form_error');
});

//Send form form index page
$("#signupForm").submit(function(e) {
    e.preventDefault();

    var url = "path/to/your/script.php"; // the script where you handle the form input.

    if ( formValidate() === true) {
      $.ajax({
             type: "POST",
             url: url,
             data: $("#signupForm").serialize(),
             success: function(data)
             {
                 $('.signup-form__submit').val('Success!').addClass('submit--success');
             },
             error: function(errorThrown){
               alert(errorThrown);
             }
           });
    } else {
      return false;
    }
});
